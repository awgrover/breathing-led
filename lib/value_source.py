"""
Copyright 2024 Alan Grover awgrover@msen.com. Licensed under GPLV3.

ValueRange classes. Composable, ask one for its value by () or .value

    # e.g. get 0 .. 1.0 from an analog input:
    mapped_pin = MapRange( AnalogInput(board.A0), from_range(100,60000) )
    print( mapped_pin() )
"""

import digitalio, analogio

from map_range import *

class ValueSource():
    """Interface for things that provide a value source"""
    def __call__(self):
        return self.value()

    def value(self):
        raise Exception("implement me to return a value")

class DigitalInput(ValueSource):
    """a board.$pin; or digitalio.DigitalInOut as INPUT
        defaults to pull-up
        `inverted=True` to treat the pull-up direction as False (which is typical for N.O. switches)
    """
    def __init__(self, pin, pull=digitalio.Pull.UP, inverted=False):
        if isinstance(pin, digitalio.DigitalInOut):
            self.pin = pin
        else:
            self.pin = digitalio.DigitalInOut(pin)
        self.pin.direction = digitalio.Direction.INPUT
        self.pin.pull = pull
        self.inverted=inverted

    def value(self):
        return (not self.pin.value) if self.inverted else self.pin.value

ct=0
class AnalogInput(ValueSource):
    """a board.$pin; or analogio.AnalogIn as INPUT
    """
    def __init__(self, pin):
        if isinstance(pin, analogio.AnalogIn):
            self.pin = pin
        else:
            self.pin = analogio.AnalogIn(pin)

    def value(self):
        return self.pin.value

class MapRange(ValueSource):
    """Map a ValueSource
        Defaults to a range of 0.0 ... 1.0
        ranges can be a range(a,b), or a pair [a,b] for non-integer ranges
            step is ignored
        ranges can be reversed: range(100,1)
    """
    def __init__(self, value_source, from_range=range(0,65535), to_range=[0.0,1.0] ):
        self.value_source=value_source
        self.from_range = [from_range.start, from_range.stop] if isinstance(from_range,range) else from_range
        self.to_range = [to_range.start, to_range.stop] if isinstance(to_range,range) else to_range

    def value(self):
        v = self.value_source()
        rez =  map_range(v, self.from_range[0], self.from_range[1], self.to_range[0],self.to_range[1] )
        if self.to_range[0] < self.to_range[1]:
            if rez < self.to_range[0]:
                rez = self.to_range[0]
            elif rez > self.to_range[1]:
                rez = self.to_range[1]
        else:
            if rez > self.to_range[0]:
                rez = self.to_range[0]
            elif rez < self.to_range[1]:
                rez = self.to_range[1]
        return rez


class ValueIfEnabled(ValueSource):
    """Given a predicate ( value-source that gives 0,1,t,f; or any callable )
    return the 2nd value-source's value.
    the default value when false is 0, override with `false_value`
    """
    def __init__(self, predicate, value_source, false_value=0):
        self.predicate=predicate
        self.value_source=value_source
        self.false_value = false_value

    def value(self):
        if self.predicate():
            return self.value_source()
        else:
            return self.false_value

class ExponentialSmooth(ValueSource):
    """Exonential average, with a "window" size (aka "beta")"""
    def __init__(self, value_source, window_size=5, initial_value=0.5):
        self.value_source=value_source
        self.beta = window_size
        self.sofar = initial_value

    def value(self):
        self.sofar = self.value_source() / self.beta + self.sofar - self.sofar / self.beta; 
        return self.sofar

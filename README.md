# Breathe

Fade-in/fade-out of a (non-addressable) LED strip, trying to get a nice, natural fade.

# Target Hardware

See the .jpg for a fritzing drawing, and .svg for the schematic. Also, an .ods for bill-of-materials.

Designed for a 12v LED strip (fairly small, 1A), using a single 12V (wall) power supply. That's why there is a buck-converter. And, a buck-converter to reduce heat build-up (vs. a voltage-regulator). You could use a higher power LED strip by increasing the power-supply output (more amps), but check the motor-driver max (the buck-converter only powers the QtPy).

Has speed, brightness, and optional ambient brightness controls.

The code should be adaptable to any circuitpython board. I chose the QtPy SAMD21 from Adafruit because it is small.

I chose a motor-driver instead of a MOSFet: it was already fully engineered for 3V use, required only soldering wires, and I only needed 1. I wasn't happy with all the clone/knockoff MOSFet driver boards, which are engineered for 5V (and no "inrush" resistor). Maybe I'll change my mind.

Note the 2 pin header. This is the ambient-sensor-enable. You can omit the ambient sensor, and then also omit the 2 pin header. It will operate fine, the brightness-pot will do brightness.

To use an ambient-sensor (phototransistor), hook SDA to GND. I provided for a 2 pin header to do that, to use a jumper to allow enabling at-will (it could also be a slide-switch). When enabled, the ambient-sensor is multiplied with the brightness-pot.

# Some Description

There are 2 main elements of the breathing pattern. 

1. I create a log map for PWM. This allows an even perceptual steps from 0 to maximum. This is particularly nice for the low-end. I wish you could get native log PWM.
2. There is another mapping to get a sine-like lead-in, and lead-out at the ends of the fade.

This code only does one thing: run a fade. So, I use time.sleep(). That's a bummer if you want to mix this code with anything else.

There is some extra code in the main loop, to allow exploring other breathing algorithms. We compared, and tweaked to arrive at the "s" one. You can use a serial-console to tell the board to try different algorithms.

And you might notice my "lib/value_source.py" classes. I keep coming back to this sort of pattern, particularly the ability to compose them for things like maprange(analoginput). I didn't need a debounce for this project, nor do I really care about averaging the pot values (for stability).

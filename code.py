""" 
Copyright 2024 Alan Grover awgrover@msen.com. Licensed under GPLV3.

Light-box:
Breathing pattern.
Brightness control, ambient brightness control, speed control

# need an m4 for code size now

motor-driver as led-strip drive
buck 12v->3v converter

"""
import board

# Pins
Strip1Pin        = board.SDA # pwm out, also board-led, so indicates alive
#Strip2Pin        = board.D12 # pwm out (reserved)
#Strip3Pin        = board.D11 # pwm out (reserved)
AmbientEnablePin = board.A3 # PU, (to ground to enable)
AmbientSensorPin = board.A0  # analog in
BrightnessPotPin = board.A1  # analog in
SpeedPotPin      = board.A2  # analog in

print(f"Start {__file__}")

import digitalio, pwmio, sys
from analogio import AnalogIn
import supervisor
import math
from value_source import *
from every.every import Every
print(Every)

SpeedMax = 10 # actually interval, msecs, so /1000!

speed_pot = MapRange( AnalogInput(SpeedPotPin), from_range=range(500,65535), to_range=[0.0, SpeedMax / 1000.0] ) # seconds
brightness_pot = ExponentialSmooth(
    MapRange( AnalogInput(BrightnessPotPin), from_range=range(300,65535) ),
    100
)
# when the brightness_sensor is enabled, give some extra gain to the pot (then clip)
extra_ambient_gain = 5.0
# if the brightness_sensor_enable jumper is "off", treat it as no effect: 1.0
brightness_sensor_enable = DigitalInput(AmbientEnablePin, inverted=True)
brightness_sensor = ExponentialSmooth(
    MapRange(AnalogInput(AmbientSensorPin),to_range=(0,1.0)), # see brightnes() for "only if enabled"
    1000 # roughly how long (steps) it takes to reach current value. 1000 ~ twice through at speed 2
)

breath_led = pwmio.PWMOut(Strip1Pin, frequency=5000, duty_cycle=0)

import time

def setup_log():
    values = []
    b = 65535
    while b >= 0:
        was = b

        values.append(b)
        b = int(b * (1.0 - 0.03))
        if was == b:
            # didn't change, so, too small of float, try -1
            b = b - 1
    values.reverse()
    return values

def setup_sine():
    values = []
    b = math.pi / 2
    was = 0

    while b >= -(math.pi / 2):
        b_sined = math.sin(b) + 1  # 0..2
        # getting 2 zeros, so do the if same skip thing
        values.append(int(b_sined * 65535 / 2))
        b = b - (math.pi / 2) / 270.0  # 270 steps, linear in radians
        # print(f"{b}")

    # values.extend( [ x for x in range(5000,0, -100) ] )
    # values.append(0)
    values.reverse()
    return values

def map_range(s, a1, a2, b1, b2):
    return b1 + ((s - a1) * (b2 - b1) / (a2 - a1))

def step_pause(scaling=1.0):
    speed = speed_pot() * scaling
    time.sleep(speed)
    return speed

_brightness_call_ct=0
def brightness():
    global _brightness_call_ct,_brightness
    if _brightness_call_ct % 10 == 0:

        # 0..1
        # this could be encoded in MapRange() etc.
        # but, more clear about the calculation?

        # Imagine a situation where the sensor is poorly positioned to reflect the environment's brightness,
        # We'd like to adjust the ambient range
        # As an approximation, if using the ambient sensor, have the manual brightness have an extra multiply and clip

        if brightness_sensor_enable():
            v = extra_ambient_gain * brightness_pot() * brightness_sensor()
            if v > 1.0:
                v = 1.0
            _brightness = v
        else:
            _brightness = brightness_pot()
    else:
        brightness_sensor()

    _brightness_call_ct += 1
    return _brightness

def breath(values, fwd=True, start_skip=0, slowness_scaling=1.0):
    # run a complete cycle in one direction

    print(f"speed {time.monotonic():.3f} {speed_pot()*1000:.3f} brightness {brightness_pot():.3f} sensor {brightness_sensor() if brightness_sensor_enable() else -1:.3f} -> {brightness():.3f}")

    if fwd:
        for i in range(start_skip, len(values)):
            breath_led.duty_cycle = int( values[i] * brightness() )
            step_pause(slowness_scaling)
    else:
        for i in range(len(values) - 1, start_skip - 1, -1):
            breath_led.duty_cycle = int( values[i] * brightness() )
            step_pause(slowness_scaling)

    return breath_led.duty_cycle  # last led value

log_brightness = setup_sine()  # setup_log()
log_brightness_2 = [x for i, x in enumerate(log_brightness) if not (i % 2)]
# print(len(log_brightness), log_brightness)
# print(len(log_brightness_2), log_brightness_2)
sine_brightness = setup_sine()  # setup_log()
sine_brightness_2 = [x for i, x in enumerate(sine_brightness) if not (i % 2)]
print(len(sine_brightness), sine_brightness)

cmd = "s" # default breatthing pattern

print("Start loop")
ct = 0
loop_start = 0
loop_duration_average=ExponentialSmooth( (lambda: time.monotonic()-loop_start), 10, initial_value=0 )
say_duration = Every(1.0)
say_duration_ct=0
say_on = Every(0.1)

while True:
    # Can try out various breathing patterns via console input
    # default is `cmd`

    loop_start = time.monotonic()

    if supervisor.runtime.serial_bytes_available:
        new_cmd = input().strip()
        if new_cmd != "":
            cmd = new_cmd
            # print(f"New: {cmd.__class__.__name__}",cmd);

    if cmd == "o": # on
        ct += 1
        breath_led.duty_cycle = int(brightness() * 65535)
        #time.sleep(0.1)
        if say_on():
            print(f"speed {speed_pot()} en {brightness_sensor_enable()} brightness {brightness_pot()} sensor {brightness_sensor()} = {brightness()}")

    elif cmd == "f":
        # linear
        for b in range(0, 65536, 100):
            breath_led.duty_cycle = b
            step_pause()
        for b in range(65535, -1, -100):
            breath_led.duty_cycle = b
            step_pause()
    elif cmd == "l":
        # log, same in/out
        breath(log_brightness, True)
        breath(log_brightness, False)

    elif cmd == "L":
        # log, but clipping ends, so not 0
        breath(log_brightness, True, 10)
        breath(log_brightness, False, 10 )
    elif cmd == "P":
        # log, but pauses
        top = breath(log_brightness, True, 20)
        # analogWrite(digitalPin, int(top * (1.0 - 0.03/3) ))
        s = step_pause()
        time.sleep(s * 30)
        breath(log_brightness, False, 10)
        time.sleep(s * 40)
        breath(log_brightness, False, 10)

    elif cmd == "P":
        # log, assymetric pauses
        top = breath(log_brightness, True, 20)
        # analogWrite(digitalPin, int(top * (1.0 - 0.03/3) ))
        s = step_pause()
        time.sleep(s * 30)
        breath(log_brightness, False, 20)
        time.sleep(s * 40)
    elif cmd == "F":
        # log, assymetric speeds
        top = breath(log_brightness, True, 20)
        # analogWrite(digitalPin, int(top * (1.0 - 0.03/3) ))
        s = step_pause()
        time.sleep(s * 50)
        breath(log_brightness_2, False, 10)
        time.sleep(s * 30)  # off time
    elif cmd == "s":
        # sine, assymetric speed
        # in (brighter)
        # The client liked this one best
        top = breath(sine_brightness, True, 0, slowness_scaling=0.75)
        s = step_pause()
        time.sleep(s * 0)  # how long to pause inhaled. s*50 is much longer

        # out (dimmer)
        breath(sine_brightness, False, 0)
        time.sleep(s * 200)  # off time.

    loop_duration = loop_duration_average()
    say_duration_ct+=1
    if say_duration():
        print(f"Loop {loop_duration/say_duration_ct*1000:.3f}")
        say_duration_ct=0
    loop_start = time.monotonic()

